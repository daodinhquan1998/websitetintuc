<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function getXoa($id,$idTinTuc)
    {
        $cmt = Comment::find($id);
        $cmt->delete();
        return redirect('admin/TinTuc/sua/'.$idTinTuc)->with('thongbao','Xóa thành công');
    }
}
