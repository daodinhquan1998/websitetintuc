<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoaiTin;
use App\Models\TheLoai;


class LoaiTinController extends Controller
{
    public function getDanhSach()
    {   
    	$loaitin = LoaiTin::all();
        $theloai = TheLoai::all();
        return view('admin/LoaiTin/danhsach', compact('loaitin','theloai'));
    }
    public function getThem()
    {
        $theloai = TheLoai::all();
        return view('admin/LoaiTin/them',compact('theloai'));
    }

    public function PostThem(Request $request)
    {
        $this->validate($request,
            [
                'Ten' => 'required|unique:LoaiTin,Ten|min:3|max:100',   
                'theloai' => 'required'
            ],
            [
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.min' =>'Tên thể loại phải có từ 3 đến 25 ký tự',
            'Ten.max' =>'Tên thể loại phải có từ 3 đến 25 ký tự'
            ]);

        $LoaiTin = new LoaiTin;
        $LoaiTin->Ten = $request->Ten;
        $LoaiTin->TenKhongDau = str_slug($request->Ten,'-');
        $LoaiTin->idTheLoai = $request->theloai;
        $LoaiTin->save();
        return redirect('admin/LoaiTin/them')->with('thongbao','Thêm thành công');

    }
    public function getSua($id)
    {   
        $loaitin = LoaiTin::find($id);
        $theloai = TheLoai::all();
    	return view('admin/LoaiTin/sua',compact('loaitin','theloai'));
    }

    public function postSua(Request $request,$id)
    {
        $this->validate($request,
            [
                'Ten' => 'required|unique:LoaiTin,Ten|min:3|max:100',   
                'theloai' => 'required'
            ],
            [
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.unique' =>'Tên loại tin đã tồn tại',
            'Ten.min' =>'Tên thể loại phải có từ 3 đến 25 ký tự',
            'Ten.max' =>'Tên thể loại phải có từ 3 đến 25 ký tự'
            ]);

        $LoaiTin = LoaiTin::find($id);
        $LoaiTin->Ten = $request->Ten;
        $LoaiTin->TenKhongDau = str_slug($request->Ten,'-');
        $LoaiTin->idTheLoai = $request->TheLoai;
        $LoaiTin->save();
        return redirect('admin/LoaiTin/sua/'.$id)->with('thongbao','Thêm thành công');
    }

    public function getXoa($id)
    {
        $loaitin = LoaiTin::find($id);
        $loaitin->delete();
        return redirect('admin/LoaiTin/danhsach')->with('thongbao','Xoa thanh cong');
    }
}
