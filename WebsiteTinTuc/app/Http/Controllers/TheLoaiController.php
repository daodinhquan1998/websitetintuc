<?php

namespace App\Http\Controllers;
use Validator; // khai báo để sử dụng validate
use Illuminate\Http\Request;
use App\Models\TheLoai;
class TheLoaiController extends Controller
{
    public function getDanhSach()
    {   
        $theloai = TheLoai::all();
    	return view('admin/TheLoai/danhsach',compact('theloai')); // compact truyền 1 mảng sang view 
    
    }
    public function getThem()
    {
    	return view('admin/TheLoai/them');
    }

    public function postThem(Request $request)
    {
       $this->validate($request,
        [
            'Ten' => 'required|min:3|max:25'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.min' =>'Tên thể loại phải có từ 3 đến 25 ký tự',
            'Ten.max' =>'Tên thể loại phải có từ 3 đến 25 ký tự'
        ]);
       $theloai = new TheLoai;
       $theloai->Ten = $request->Ten;
       $theloai->TenKhongDau = str_slug($request->Ten,'-');
       $theloai->save();
       return redirect('admin/TheLoai/them')->with('thongbao','Thêm thành công');// with giống như khởi tạo 1 section có tên là "thongbao"
    }
    public function getSua($id)
    {
        $theloai = TheLoai::find($id);
    	return view('admin/TheLoai/sua',compact('theloai'));
    }

    public function postSua(Request$request,$id) 
    {
        $theloai = TheLoai::find($id);
        $this->validate($request,
            [
                'Ten' =>'required| unique:Theloai,Ten|min:3|max:100'
            ],
            [
                'Ten.required' => 'Bạn chưa nhập tên thể loại',
                'Ten.unique' => 'Tên thể loại đã tồn tại', 
            ]);
        $theloai->Ten = $request->Ten;
        $theloai->TenKhongDau = str_slug($request->Ten,'-');
        $theloai->save();

        return redirect('admin/TheLoai/sua/'.$id)->with('thongbao','Sua thanh cong');
    }

    public function getXoa($id) 
    {
        $theloai = TheLoai::find($id);
        $theloai->delete();
        return redirect('admin/TheLoai/danhsach')->with('thongbao','Xóa thành công');
    }
}