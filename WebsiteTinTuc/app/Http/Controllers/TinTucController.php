<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoaiTin;
use App\Models\TheLoai;
use App\Models\TinTuc;
use App\Models\Comment;
use Validator;

class TinTucController extends Controller
{
    public function getDanhSach()
    {
        $tintuc = TinTuc::orderBy('id','DESC')->get();
        $loaitin = LoaiTin::all();
        $theloai = TheLoai::all();
        return view('admin/TinTuc/danhsach',compact('tintuc','loaitin','theloai'));
    }
    public function getThem()
    {
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        return view('admin/TinTuc/them',compact('theloai','loaitin'));
    }

    public function PostThem(Request $request)
    {
        $this->validate($request,
        [
            'LoaiTin' => 'required',
            'TieuDe' =>'required| min :3 |max :30|Unique:TinTuc,TieuDe',
            'TomTat'=>'required',
            'NoiDung'=>'required'
        ],
        [
            'LoaiTin.required' =>'Bạn chưa chọn loại tin',
            'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
            'TieuDe.min'=>'Tiêu đề phải có ít nhất 3 kí tự',
            'TieuDe.max'=>'Tiêu đề có tối đa 30 ký tự',
            'TieuDe.unique'=>'Tiêu đề đã tồn tại',
            'TomTat.required'=>'Bạn chưa nhập tóm tắt',
            'NoiDung.required'=>'Bạn chưa nhập nội dung'
        ]);

        $tintuc = new TinTuc;
        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TieuDeKhongDau = str_slug($request->TieuDe,'-');
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->NoiDung =$request->NoiDung;
        $tintuc->NoiBat = $request->NoiBat;

        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi !='png')
            {
                return redirect('admin/TinTuc/them')->with('loi','File đưa lên không hợp lệ');
            }
            $name = $file->getClientOriginalName();
            $Hinh = str_random(4)."_". $name;
            while(file_exists('upload/tintuc/'.$Hinh))
            {
                $Hinh = str_random(4)."_". $name;
            }
            $file->move("upload/tintuc",$Hinh);
            $tintuc->Hinh = $Hinh;
        }
        else
        {
            $request->Hinh = "";
        }
        $tintuc->save();
        return redirect('admin/TinTuc/them')->with('thanhcong','Thêm tin tức thành công');
    }
    public function getSua($id)
    {
        $tintuc = TinTuc::find($id);
        $loaitin = LoaiTin::all();
        $theloai = TheLoai::all();
        return view('admin/TinTuc/sua',compact('tintuc','loaitin','theloai'));
    }

    public function postSua(Request $request,$id)
    {
        $tintuc = TinTuc::find($id);
        $this->validate($request,
        [
            'LoaiTin' => 'required',
            'TieuDe' =>'required| min :3 |max :30',
            'TomTat'=>'required',
            'NoiDung'=>'required'
        ],
        [
            'LoaiTin.required' =>'Bạn chưa chọn loại tin',
            'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
            'TieuDe.min'=>'Tiêu đề phải có ít nhất 3 kí tự',
            'TieuDe.max'=>'Tiêu đề có tối đa 30 ký tự',
            'TomTat.required'=>'Bạn chưa nhập tóm tắt',
            'NoiDung.required'=>'Bạn chưa nhập nội dung'
        ]);

        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TieuDeKhongDau = str_slug($request->TieuDe,'-');
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->NoiDung =$request->NoiDung;
        $tintuc->NoiBat = $request->NoiBat;

        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi !='png')
            {
                return redirect('admin/TinTuc/them')->with('loi','File đưa lên không hợp lệ');
            }
            $name = $file->getClientOriginalName();
            $Hinh = str_random(4)."_". $name;
            while(file_exists('upload/tintuc/'.$Hinh))
            {
                $Hinh = str_random(4)."_". $name;
            }
            $file->move("upload/tintuc",$Hinh);
            $tintuc->Hinh = $Hinh;
        }
        $tintuc->save();
         return redirect('admin/TinTuc/sua/'.$id)->with('thongbao','Sửa tin tức thành công');

    }

    public function getXoa($id)
    {
        $tintuc = TinTuc::find($id);
        $tintuc->delete();
        return redirect('admin/TinTuc/danhsach')->with('thongbao','Xóa thành công');
    }
}
