<?php

namespace App\Http\Controllers;

use App\Models\TinTuc;
use Illuminate\Http\Request;
use App\Models\Slide;

class slideController extends Controller
{
    public function getDanhSach()
    {
        $slide = Slide::all();
    	return view('admin/slide/danhsach',compact('slide'));
    }
    public function getThem()
    {
    	return view('admin/slide/them');
    }

    public function postThem(Request $request)
    {
        $this->validate($request,
            [
                'Ten' => 'required',
                'Link' =>'required',
                'NoiDung'=>'required'
            ],
            [
                'Ten.required' =>'Bạn chưa chọn loại tin',
                'Link.required'=>'Bạn chưa nhập tiêu đề',
                'NoiDung.required'=>'Bạn chưa nhập nội dung'
            ]);

        $slide = new Slide;
        $slide->Ten = $request->Ten;
        $slide->Link = $request->Link;
        $slide->NoiDung = $request->NoiDung;

        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi !='png')
            {
                return redirect('admin/Slide/them')->with('loi','File đưa lên không hợp lệ');
            }
            $name = $file->getClientOriginalName();
            $Hinh = str_random(4)."_". $name;
            while(file_exists('upload/Slide/'.$Hinh))
            {
                $Hinh = str_random(4)."_". $name;
            }
            $file->move("upload/Slide",$Hinh);
            $slide->Hinh = $Hinh;
        }
        else
        {
            $request->Hinh = "";
        }
        $slide->save();
        return redirect('admin/Slide/them')->with('thongbao','Thêm Slide thành công');
    }
    public function getSua()
    {
    	return view('admin/slide/sua');
    }
}
