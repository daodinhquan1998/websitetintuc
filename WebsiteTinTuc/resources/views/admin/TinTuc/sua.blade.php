@extends('admin/layout/index')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa
                            <small>{{$tintuc->TieuDe}}</small>
                        </h1>
                    </div>
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif

                        @if(session('loi'))
                            <div class="alert alert-danger">
                                {{session('loi')}}
                            </div>
                        @endif
                        <form action="admin/TinTuc/sua/{{$tintuc->id}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" name="TheLoai" id="TheLoai">
                                    @foreach($theloai as $tl)
                                    <option
                                    @if($tintuc->loaitin->theloai->id == $tl->id)
                                        {{"selected"}}
                                    @endif
                                    value="{{$tl->id}}">{{$tl->Ten}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <label>Loại Tin</label>
                            <select class="form-control" name ="LoaiTin" id="LoaiTin">
                                    @foreach($loaitin as $lt)
                                    <option
                                    @if($tintuc->loaitin->theloai->id == $tl->id)
                                        {{"selected"}}
                                    @endif
                                    value="{{$lt->id}}">{{$lt->Ten}}
                                    </option>
                                    @endforeach
                                </select>
                            <br>
                            <div class="form-group">
                                <label>Tiêu Đề</label>
                                <input class="form-control" name="TieuDe" value="{{$tintuc->TieuDe}}"/>
                            </div>
                            <div class="form-group">
                                <label>Tóm Tắt</label>
                                <textarea id="tomtat" class="form-control ckeditor" name="TomTat" rows="3">
                                {{$tintuc->TomTat}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Nội Dung</label>
                                <textarea id="noidung" class="form-control ckeditor" name="NoiDung" rows="5">
                                {{$tintuc->NoiDung}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Hình</label>
                                <img width="50px" src="upload/tintuc/{{$tintuc->Hinh}}">
                                <input class="form-control" name="Hinh" type="file" />
                            </div>
                            <div class="form-control">
                                <label>Nổi Bật</label>
                                <label class="radio-inline">
                                    <input value="0" type="radio" name="NoiBat"
                                    @if($tintuc->NoiBat == 0)
                                        {{"checked"}}
                                    @endif
                                    > Không
                                </label>
                                <label class="radio-inline">
                                    <input value="1" type="radio" name="NoiBat"
                                    @if($tintuc->NoiBat == 1)
                                        {{"checked"}}
                                    @endif>
                                     Có
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Bình luận
                            <small></small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr align="center">
                            <th>ID</th  >
                            <th>Người dùng</th>
                            <th>Nội dung</th>
                            <th>Ngày đăng</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tintuc->comment as $cmt)
                            <tr class="odd gradeX" align="center">
                                <td>{{$cmt->id}}</td>
                                <td>{{$cmt->user->name}}</td>
                                <td>{{$cmt->NoiDung}}</td>
                                <td>{{$cmt->created_at}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                    <a href="admin/comment/xoa/{{$cmt->id}}/{{$tintuc->id}}"> Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                <!--end row   >
            </div>
            <!-- /.container-fluid -->
</div>
@endsection

@section('script')
    <script>
    $(document).ready(function(){
        var idTheLoai = $('#TheLoai').val();
            $.get("admin/ajax/loaitin/"+idTheLoai, function(data){
                $("#LoaiTin").html(data);
            });
        $("#TheLoai").change(function(){
            var idTheLoai = $(this).val();
            $.get("admin/ajax/loaitin/"+idTheLoai, function(data){
                $("#LoaiTin").html(data);
            });
        });
    });
</script>
@endsection
