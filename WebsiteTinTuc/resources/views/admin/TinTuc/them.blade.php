@extends('admin/layout/index')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thêm
                            <small>Tin Tức</small>
                        </h1>
                    </div>
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif

                        @if(session('loi'))
                            <div class="alert alert-danger">
                                {{session('loi')}}
                            </div>
                        @endif
                        <form action="admin/TinTuc/them" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" name="TheLoai" id="TheLoai">
                                    @foreach($theloai as $tl)
                                    <option value="{{$tl->id}}">{{$tl->Ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label>Loại Tin</label>
                            <select class="form-control" name ="LoaiTin" id="LoaiTin">
                                    @foreach($loaitin as $lt)
                                    <option value="{{$lt->id}}">{{$lt->Ten}}</option>
                                    @endforeach
                                </select>
                            <br>
                            <div class="form-group">
                                <label>Tiêu Đề</label>
                                <input class="form-control" name="TieuDe"/>
                            </div>
                            <div class="form-group">
                                <label>Tóm Tắt</label>
                                <textarea id="tomtat" class="form-control ckeditor" name="TomTat" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Nội Dung</label>
                                <textarea id="noidung" class="form-control ckeditor" name="NoiDung" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Hình</label>
                                <input class="form-control" name="Hinh" type="file" />
                            </div>
                            <div class="form-control">
                                <label>Nổi Bật</label>
                                <label class="radio-inline">
                                    <input value="0" type="radio" name="NoiBat"> Không
                                </label>
                                <label class="radio-inline">
                                    <input value="1" type="radio" name="NoiBat"> Có
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
</div>
@endsection

@section('script')
    <script>
    $(document).ready(function(){
        var idTheLoai = $('#TheLoai').val();
            $.get("admin/ajax/loaitin/"+idTheLoai, function(data){
                $("#LoaiTin").html(data);
            });
        $("#TheLoai").change(function(){
            var idTheLoai = $(this).val();
            $.get("admin/ajax/loaitin/"+idTheLoai, function(data){
                $("#LoaiTin").html(data);
            });
        });
    });
</script>
@endsection