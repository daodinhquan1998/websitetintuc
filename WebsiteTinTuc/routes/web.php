<?php
use App\Models\Theloai;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('thu',function() {
	$theloai = App\Models\TheLoai::find(2);
	foreach ($theloai->loaitin as $loaitin)
	{
		echo $loaitin->Ten."<br>";
	}
});

Route::get('test',function() {
	return view('admin/TheLoai/danhsach');
});

Route::group(['prefix'=>'admin'],function(){

	Route::group(['prefix'=>'TheLoai'],function(){
		Route::get('danhsach','TheLoaiController@getDanhSach');

		Route::get('sua/{id}','TheLoaiController@getSua');
		Route::post('sua/{id}','TheLoaiController@postSua');

		Route::get('them','TheLoaiController@getThem');
		Route::post('them','TheLoaiController@postThem');
		Route::get('xoa/{id}','TheLoaiController@getXoa');
	});

	Route::group(['prefix'=>'LoaiTin'],function(){
		Route::get('danhsach','LoaiTinController@getDanhSach');

		Route::get('sua/{id}','LoaiTinController@getSua');
		Route::post('sua/{id}','LoaiTinController@postSua');

		Route::get('them','LoaiTinController@getThem');
		Route::post('them','LoaiTinController@postThem');
		Route::get('xoa/{id}','LoaiTinController@getXoa');
	});

	Route::group(['prefix'=>'TinTuc'],function(){
		Route::get('them','TinTucController@getThem');
		Route::post('them','TinTucController@postThem');
		Route::get('danhsach','TinTucController@getDanhSach');
		Route::get('sua/{id}','TinTucController@getSua');
		Route::post('sua/{id}','TinTucController@postSua');
        Route::get('xoa/{id}','TinTucController@getXoa');
	});

    Route::group(['prefix'=>'comment'],function(){
        Route::get('xoa/{id}/{idTinTuc}','CommentController@getXoa');
    });

	Route::group(['prefix'=>'ajax'],function(){
		Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin');
	});

	Route::group(['prefix'=>'user'],function(){
		Route::get('them','userController@getThem');
		Route::get('danhsach','userController@getDanhSach');
		Route::get('sua','userController@getSua');
	});

	Route::group(['prefix'=>'Slide'],function(){
		Route::get('them','slideController@getThem');
		Route::post('them','slideController@postThem');
		Route::get('danhsach','slideController@getDanhSach');
		Route::get('sua','slideController@getSua');
	});
});


